# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .health_moves_remediar import *
from .report import *

def register():
    Pool.register(
        Product, 
        module='health_moves_remediar_fiuner', type_='model')
    Pool.register(
        ReportMovimientos,
        module='health_moves_remediar_fiuner', type_='report')
