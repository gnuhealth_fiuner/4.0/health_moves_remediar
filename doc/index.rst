﻿GNU Health z_movimientos module
###############################

Funtionality to import argentine Remediar Medicines list. Allows to add more products on a simple csv, and later executing the generate_remediar_register.py, fillout a xml list with the template, products, categories, and medicines, ready to import to a gnuhealth database.

It also provides a report to export the stock move of Remediar Products (#TODO improve)
